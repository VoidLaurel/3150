#include <stdio.h>      // required by printf()
#include <sys/types.h>  // required by getpid()
#include <unistd.h>     // required by getpid(), fork()

int main(void) {
	int result;

	printf("before fork ...\n");
	result = fork();
	printf("result = %d\n", result);

	if(result == 0) {
		sleep(1);
//		printf("I'm the child\n");
		printf("Child: My PID is %d\n", getpid());
		printf("Child: I terminated\n");
	}
	else {
		sleep(1);
//		printf("I'm the parent\n");
		printf("Parent: My PID is %d\n", getpid());
		printf("Parent: I terminated\n");
	}


	return 0;
}
